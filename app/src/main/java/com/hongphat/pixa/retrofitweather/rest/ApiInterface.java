package com.hongphat.pixa.retrofitweather.rest;

import com.hongphat.pixa.retrofitweather.model.LocationResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Admin on 9/14/2016.
 */
public interface ApiInterface {

    @GET("/data/2.5/weather")
    Call<LocationResponse> getLocation(@Query("q") String place ,@Query("appid") String apiKey);

}
