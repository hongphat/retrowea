package com.hongphat.pixa.retrofitweather.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.hongphat.pixa.retrofitweather.R;
import com.hongphat.pixa.retrofitweather.model.LocationResponse;
import com.hongphat.pixa.retrofitweather.rest.ApiClient;
import com.hongphat.pixa.retrofitweather.rest.ApiInterface;

import org.w3c.dom.Text;

import java.text.Normalizer;
import java.util.List;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    TextView idText, cityText;
    private final static String API_KEY = "712d0edf8732db2a90813e3d34be21fb";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (API_KEY.isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please obtain your API KEY", Toast.LENGTH_LONG).show();
            return;
        }
        idText = (TextView)findViewById(R.id.id);
        cityText = (TextView)findViewById(R.id.city);


        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<LocationResponse> call = apiService.getLocation("London", API_KEY);
        call.enqueue(new Callback<LocationResponse>() {
            @Override
            public void onResponse(Call<LocationResponse> call, Response<LocationResponse> response) {
                int statusCode = response.code();
                Log.i("App info", "Response");
                if (response.body() == null) {
                    Log.i("App info", "Body null");
                }
                idText.setText(response.body().id);
                cityText.setText(response.body().name);
            }

            @Override
            public void onFailure(Call<LocationResponse> call, Throwable t) {
                Log.i("App info", "Fail");
            }
        });
    }
}
